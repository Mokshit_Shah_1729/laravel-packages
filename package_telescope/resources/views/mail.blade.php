<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Send Mail - Telescope</title>
</head>
<body>
    
    <form action="{{ route('send') }}" method="POST">
        @csrf
        <h1>Send mail</h1>
        <h2>To:</h2>
        <input type="text" name="email">
        @error('email')
            <p>{{ $message }}</p>
        @enderror
        <button type="submit">Send</button>
    </form>
</body>
</html>