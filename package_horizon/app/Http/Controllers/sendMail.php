<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendingMail;

class sendMail extends Controller
{
    public function index(){
        return view('mail');
    }

    public function send(){
        request()->validate(['email'=>'required|email']);
        Mail::to(request('email'))->send(new SendingMail());
        return redirect()->back();
    }
}
