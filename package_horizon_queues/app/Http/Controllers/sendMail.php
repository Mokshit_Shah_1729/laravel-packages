<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendingMail;

class sendMail extends Controller
{
    public function index(){
        return view('mail');
    }

    public function send(){

        $toemails = [
            "1@gmail.com",
            "2@gmail.com",
            "3@gmail.com",
            "4@gmail.com",
            "5@gmail.com",
            "6@gmail.com",
            "7@gmail.com",
            "8@gmail.com",
            "9@gmail.com",
            "10@gmail.com",
            "11@gmail.com",
            "12@gmail.com",
            "13@gmail.com",
            "14@gmail.com",
            "15@gmail.com",
            "16@gmail.com",
            "17@gmail.com",
            "18@gmail.com",
            "19@gmail.com",
            "20@gmail.com",
            "21@gmail.com",
            "22@gmail.com",
            "23@gmail.com",
            "24@gmail.com",
            "25@gmail.com",
            "26@gmail.com",
            "27@gmail.com",
            "28@gmail.com",
            "29@gmail.com",
            "30@gmail.com",
            "31@gmail.com",
            "32@gmail.com",
            "33@gmail.com",
            "34@gmail.com",
            "35@gmail.com",
            "36@gmail.com",
            "37@gmail.com",
            "38@gmail.com",
            "39@gmail.com",
            "40@gmail.com",
        ];

        foreach ($toemails as $key => $email) {
            Mail::to($email)->send(new SendingMail());
        }

        return redirect()->back();
    }
}
