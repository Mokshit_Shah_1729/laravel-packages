<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Device;

class DeviceController extends Controller
{
    function list($id = null){
        return $id?Device::find($id):Device::all();
    }

    function add(Request $request){

        $device = new Device;
        $device->name = $request->name;
        $device->device = $request->device;
        $result = $device->save();
        if($result){
            return ["Result"=>"Success"];
        }
        else{
            return ["Result"=>"Failure"];
        }

    }

    function update(Request $request){

        $device = Device::find($request->id);
        $device->name = $request->name;
        $device->device = $request->device;
        $result = $device->save();
        if($result){
            return ["Result"=>"Success"];
        }
        else{
            return ["Result"=>"Failure"];
        }

    }

    function search($name){
        $result = Device::where("name","like","%".$name."%")->get();
        if(count($result)){
            return $result;
        }
        else{
            return ["Result"=>"No data found."];
        }
    }

    function delete($id){

        $device = Device::find($id);
        $result = $device->delete();
        if($result){
            return ["Result"=>"Success"];
        }
        else{
            return ["Result"=>"Failure"];
        }
    }
}
